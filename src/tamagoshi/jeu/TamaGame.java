package tamagoshi.jeu;

import tamagoshi.tamagoshis.Tamagoshi;
import tamagoshi.tamagoshis.GrosJoueur;
import tamagoshi.tamagoshis.GrosMangeur;
import tamagoshi.util.Utilisateur;

import java.util.ArrayList;
import java.util.Random;

public class TamaGame {
    private ArrayList<Tamagoshi> tamagoshiDepart;
    private ArrayList<Tamagoshi> tamagoshiEnCourse;
    private Random random = new Random();

    public TamaGame() {
        this.initialisation();
    }

    private void initialisation() {
        this.tamagoshiDepart = new ArrayList<Tamagoshi>();
        this.tamagoshiEnCourse = new ArrayList<Tamagoshi>();
        System.out.println("Veuillez entrer le nombre de Tamagoshi : ");
        int nbTamagoshi = Integer.parseInt(Utilisateur.saisieClavier());
        for (int i = 1; i < nbTamagoshi + 1; i++) {
            System.out.println("Nom du Tamagohi n°" + i + " :");
            String name = Utilisateur.saisieClavier();
            Tamagoshi t;
            if (random.nextBoolean()) {
                t = new GrosJoueur(name);
            } else {
                t = new GrosMangeur(name);
            }

            this.tamagoshiDepart.add(t);
            this.tamagoshiEnCourse.add(t);
        }
    }

    public void play() {
        int nbTour = 0;
        while (this.tamagoshiEnCourse.size() != 0) {
            nbTour++;
            System.out.println("------Tour N°" + nbTour + "------\n");
            for (Tamagoshi t : this.tamagoshiEnCourse
            ) {
                System.out.println(t.getClass().getSimpleName());
                System.out.println(Tamagoshi.class.getSimpleName());
            t.parle();
            }

            // Nourrir
            System.out.println("\n Nourrir quel tamagoshi ? :\n");
            listeChoixTamagochi();
            try {
                this.tamagoshiEnCourse.get(Integer.parseInt(Utilisateur.saisieClavier())).mange();
            } catch (IndexOutOfBoundsException e) {
                System.out.println("\n  Aucun tamagoshi nourri\n");
            }

            // Fun
            System.out.println("\n Jouer avec quel tamagoshi ? :\n");
            listeChoixTamagochi();
            try {
                this.tamagoshiEnCourse.get(Integer.parseInt(Utilisateur.saisieClavier())).fun();
            } catch (IndexOutOfBoundsException e) {
                System.out.println("\n Aucun tamagoshi a jouer \n");
            }

            this.tamagoshiEnCourse.removeIf(t -> (!t.consommeEnergieFun()) || (Tamagoshi.atteintAgeLimite(t)));
        }
        this.resultat();
    }

    private void listeChoixTamagochi() {
        for (int i = 0; i < this.tamagoshiEnCourse.size(); i++) {
            System.out.print(this.tamagoshiEnCourse.get(i).getName() + "(" + i + ")     ");
        }
        System.out.println("\n");
    }

    private double score() {
        double somme = 0;
        for (Tamagoshi t : this.tamagoshiDepart
        ) {
            somme += t.getAge();
        }
        return somme / (Tamagoshi.lifeTime * this.tamagoshiDepart.size());
    }

    private void resultat() {
        System.out.println("\n--------Bilan--------");
        for (Tamagoshi t : this.tamagoshiDepart
        ) {
            if (t.getAge() == Tamagoshi.lifeTime) {
                System.out.println(t.getName() + " qui était un " + t.getClass().getSimpleName() + " à bien survécu et vous remercie :)");
            } else {
                System.out.println(t.getName() + " qui était un " + t.getClass().getSimpleName() + " n'est pas arrivé au bout et ne vous félicite pas :(");
            }
        }
        System.out.println("\n Niveau de difficulté = " + this.tamagoshiDepart.size() + " , score obtenu :" + this.score() * 100 + "%");
    }

    public static void main(String[] args) {
        TamaGame jeu = new TamaGame();
        jeu.play();
    }
}
