package tamagoshi.tamagoshis;

public class GrosJoueur extends Tamagoshi {

    public GrosJoueur(String name) {
        super(name);
    }

    @Override
    public boolean consommeEnergieFun() {
        AgePlus();
        EnergyMoins();
        FunMoins();
        FunMoins();
        if (getEnergy()<=0) {
            System.out.println(getName()+" : Je suis KO: Arrrgh !");
            return false;
        } else {
            return true;
        }
    }
}
