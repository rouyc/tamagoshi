package tamagoshi.tamagoshis;

import java.util.Random;

public class Tamagoshi {
    private int age;
    private final int maxEnergy;
    private int energy;
    private final int maxFun;
    private int fun;
    private final String name;
    public static int lifeTime = 10;
    private final java.util.Random random;

    public Tamagoshi(String name) {
        this.name = name;
        this.age = 0;
        this.random = new Random();
        this.maxEnergy = random.nextInt(4)+5;
        this.energy = random.nextInt(4)+3;
        this.maxFun = random.nextInt(4)+5;
        this.fun = random.nextInt(4)+3;
    }

    public boolean parle() {
        if(this.energy>4 & this.fun>4) {
            System.out.println(this.name + " : Tout va bien !");
            return true;
        }
        else if(this.energy<=4 && this.fun>4){
            System.out.println(this.name + " : Je suis affamé !");
            return false;
        } else if(this.energy>4) {
            System.out.println(this.name + " : Je m'ennuie à mourir !");
            return false;
        } else {
            System.out.println(this.name + " : Je suis affamé et je m'ennuie à mourrir !");
            return false;
        }
    }

    public boolean mange() {
        if (this.energy<this.maxEnergy) {
            this.energy += random.nextInt(2)+1;
            System.out.println(this.name+" : Miam c'est bon !");
            return true;
        } else {
            System.out.println(this.name+" : J'ai pas faim !");
            return false;
        }
    }

    public boolean fun() {
        if (this.fun<this.maxFun) {
            this.fun += random.nextInt(2)+1;
            System.out.println(this.name+" : On se marre !");
            return true;
        } else {
            System.out.println(this.name+" : Laisse moi tranquille, je bouquine !!");
            return false;
        }
    }

    public boolean consommeEnergieFun() {
        this.age++;
        this.energy--;
        this.fun--;
        if (this.energy<=0) {
            System.out.println(this.name+" : Je suis KO: Arrrgh !");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String toString() {
        return "\nTamagoshi{" +
                "age=" + age +
                ", maxEnergy=" + maxEnergy +
                ", energy=" + energy +
                ", maxFun=" + maxFun +
                ", fun=" + fun +
                ", name='" + name + '\'' +
                ", random=" + random +
                '}';
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public static boolean atteintAgeLimite(Tamagoshi t) {
        if (t.age >= Tamagoshi.lifeTime) {
            System.out.println("Je suis vieux, adieu.\n");
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        Tamagoshi t1 = new Tamagoshi("Pierre");
        System.out.println(t1);
        Tamagoshi t2 = new Tamagoshi("Paul");
        System.out.println(t2);
        Tamagoshi t3 = new Tamagoshi("Jacques");
        System.out.println(t3);
    }

    public void AgePlus() {
        this.age++;
    }

    public void EnergyMoins() {
        this.energy--;
    }

    public void FunMoins() {
        this.fun--;
    }

    public int getEnergy() {
        return energy;
    }
}
