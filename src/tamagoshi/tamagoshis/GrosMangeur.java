package tamagoshi.tamagoshis;

public class GrosMangeur extends Tamagoshi {

    public GrosMangeur(String name) {
        super(name);
    }

    @Override
    public boolean consommeEnergieFun() {
        AgePlus();
        EnergyMoins();
        EnergyMoins();
        FunMoins();
        if (getEnergy()<=0) {
            System.out.println(getName()+" : Je suis KO: Arrrgh !");
            return false;
        } else {
            return true;
        }
    }
}
